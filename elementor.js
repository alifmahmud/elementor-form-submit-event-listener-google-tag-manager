<script>
(function() {
    var origXMLHttpRequest = XMLHttpRequest;
    XMLHttpRequest = function() {
        var requestURL;
        var requestMethod;

        var xhr = new origXMLHttpRequest();
        var origOpen = xhr.open;
        var origSend = xhr.send;

        xhr.open = function(method, url) {
            requestURL = url;
            requestMethod = method;
            return origOpen.apply(this, arguments);
        };

        xhr.send = function(data) {
            if (/.+\/admin-ajax\.php/.test(requestURL)) {
                xhr.addEventListener('load', function() {
                    if (xhr.readyState === 4 && xhr.status === 200) {
                        try {
                            var response = JSON.parse(xhr.responseText);

                            if (response.success && (data instanceof FormData)) {
                                var userData = {};

                                data.forEach(function(value, key) {
                                    // Modify keys for form fields and assign values
                                    var formattedKey = key.replace('form_fields[', '').replace(']', '');
                                    userData[formattedKey] = value;
                                });

                                if(userData.action === "elementor_pro_forms_send_form") {
                                    window.dataLayer = window.dataLayer || [];
                                    window.dataLayer.push({
                                        event: 'dl_form_submit',
                                        userData: userData
                                    });
                                    localStorage.setItem("userData", JSON.stringify(userData));
                                }
                            }
                        } catch (e) {
                            console.error('Error parsing response:', e);
                        }
                    }
                });
            }

            return origSend.apply(this, arguments);
        };

        return xhr;
    };
})();
</script>
