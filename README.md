# Elementor Form Event Listener for GTM - Alif Mahmud

This script is designed to track form submission events in WordPress websites that use the `admin-ajax.php` endpoint. By using this script within **Google Tag Manager (GTM)**, you can monitor form submissions, extract submitted form data, and send custom events to the GTM dataLayer for advanced tracking and analytics purposes.

---

## How It Works
The script intercepts and extends the behavior of the `XMLHttpRequest` object to track AJAX-based form submissions, specifically those targeting the `admin-ajax.php` endpoint. When a form is submitted:

1. It listens to all AJAX requests and filters those targeting `admin-ajax.php`.
2. Extracts form data and response information.
3. Pushes a custom event (`dl_form_submit`) and the extracted user data to the GTM dataLayer.
4. Optionally stores the extracted user data in `localStorage` for further use.

---

## Features
- **Event Tracking:** Captures form submissions and pushes data to the GTM dataLayer.
- **Form Field Parsing:** Extracts and formats form fields from the `FormData` object.
- **AJAX Monitoring:** Tracks only `admin-ajax.php` requests for efficiency.
- **Error Handling:** Logs errors gracefully in the browser console.
- **LocalStorage Support:** Stores the extracted data in the browser's local storage for debugging or additional use.

---

## Installation

### Step 1: Add Script to GTM
1. Open your **Google Tag Manager** account.
2. Create a new **Tag**:
   - Tag Type: **Custom HTML**.
   - Copy and paste the script into the HTML editor.
3. Set the tag's **Trigger** to fire on pages where form submissions occur (e.g., all pages or specific pages with forms).

### Step 2: Configure GTM Triggers
1. Create a **Custom Event Trigger**:
   - Event Name: `dl_form_submit`.
   - This trigger will fire whenever the `dl_form_submit` event is pushed to the dataLayer.

### Step 3: Create Variables for User Data
Define GTM variables to capture form data fields from the `userData` object in the dataLayer. For example:
   - Variable Name: `Form Field - Name`
   - Variable Type: `Data Layer Variable`
   - Data Layer Variable Name: `userData.name` (replace `name` with the actual form field key you want to track).

---

## Data Layer Event: `dl_form_submit`

When a form submission is detected, the following object is pushed to the `dataLayer`:

```javascript
{
    event: 'dl_form_submit',
    userData: {
        name: 'John Doe',
        email: 'john.doe@example.com',
        // Additional form fields
    }
}
```

### Example Fields Captured
- `name`: User's name (if the form field exists).
- `email`: User's email address (if the form field exists).
- Any other custom fields submitted via the form.

---

## Script Breakdown

### Key Functions
1. **Intercept `XMLHttpRequest` Open & Send:**
   - Hooks into `open()` and `send()` methods to capture AJAX requests targeting `admin-ajax.php`.

2. **Filter Relevant Requests:**
   - Checks if the request URL matches `/admin-ajax.php`.

3. **Extract Form Data:**
   - Parses `FormData` objects and reformats keys (e.g., removing `form_fields[]` wrappers).

4. **Push Event to GTM dataLayer:**
   - Pushes a custom event (`dl_form_submit`) with the extracted user data.

5. **Store in LocalStorage:**
   - Stores the parsed `userData` object locally for debugging or other use cases.

### Error Handling
The script includes error handling to catch and log issues during parsing or form submission.

---

## Notes & Considerations
- **Compatibility:** This script assumes form submissions are sent using `admin-ajax.php` and include a `FormData` object. Ensure your forms meet this requirement.
- **Custom Field Mapping:** You may need to adjust the key parsing logic based on your form's field naming conventions.
- **Data Privacy:** If storing sensitive user data in `localStorage`, ensure compliance with privacy regulations (e.g., GDPR).

---

## Troubleshooting
- **No Events in GTM Preview:**
  - Verify that the form submission uses AJAX and targets `admin-ajax.php`.
  - Check browser console for errors.
- **Missing Form Fields:**
  - Ensure form fields are part of the `FormData` object sent in the AJAX request.

---

By implementing this script, you can enhance your form submission tracking capabilities and gain deeper insights into user interactions on your website.